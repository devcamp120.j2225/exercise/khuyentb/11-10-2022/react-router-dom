import Footer from "../components/footer/Footer"
import Header from "../components/header/Header"
import SecondLayout from "../components/secondpage/SecondPage"

const SecondPage = () => {
    return (
        <>
            <Header></Header>

            <SecondLayout></SecondLayout>

            <Footer></Footer>
        </>
    )
}

export default SecondPage;