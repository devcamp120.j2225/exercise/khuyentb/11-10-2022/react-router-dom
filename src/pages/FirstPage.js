import Footer from "../components/footer/Footer"
import Header from "../components/header/Header"
import FirstLayout from "../components/firstpage/FirstPage"

const FirstPage = () => {
    return (
        <>
            <Header></Header>

            <FirstLayout></FirstLayout>

            <Footer></Footer>
        </>
    )
}

export default FirstPage;