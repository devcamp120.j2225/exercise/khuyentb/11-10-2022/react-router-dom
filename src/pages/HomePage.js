import Footer from "../components/footer/Footer"
import Header from "../components/header/Header"
import Home from "../components/home/Home"

const HomePage = () => {
    return (
        <>
            <Header></Header>

            <Home></Home>

            <Footer></Footer>
        </>
    )
}

export default HomePage;