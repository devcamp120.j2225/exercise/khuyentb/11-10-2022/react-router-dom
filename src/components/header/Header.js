const Header = () => {
    return (
        <div style={{ padding: "20px" }}>
            Header
            <ul style={{ listStyleType: "none" }}>
                <li ><a href="/">Home</a></li>
                <li ><a href="/firstpage">First Page</a></li>
                <li ><a href="/secondpage/1234">Second Page</a></li>
                <li ><a href="/thirdpage">Third Page</a></li>
            </ul>
            <hr></hr>

        </div>
    )
}

export default Header;